# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp.http import request
import logging
import socket

class res_partner(models.Model):

    _inherit = 'res.partner'
    _logger = logging.getLogger(__name__)

    # ------------------------ METHODS  OVERWRITTEN ---------------------------

    @api.model
    def create(self, values):
        result = super(res_partner, self).create(values)
        if result.supplier is True:
            new_partner_is_supplier = True
            link_url = self._get_base_url() + \
                "web?id=" + str(result.id) + \
                "&view_type=form&model=res.partner" + \
                "#id=" + str(result.id) + \
                "&view_type=form&model=res.partner" + \
                "&menu_id=" + \
                str(self.env.ref('account.menu_account_supplier').id) + \
                "&action=" + \
                str(self.env.ref('base.action_partner_supplier_form').id)
        else:
            new_partner_is_supplier = False
            link_url = self._get_base_url() + \
                "web?id=" + str(result.id) + \
                "&view_type=form&model=res.partner" + \
                "#id=" + str(result.id) + \
                "&view_type=form&model=res.partner" + \
                "&menu_id=" + \
                str(self.env.ref('account.menu_account_customer').id) + \
                "&action=" + \
                str(self.env.ref('base.action_partner_customer_form').id)
        ctx = self.env.context.copy()
        ctx.update({'link_url': link_url})
        template = self.env.ref(
            'partner_create_notification.res_partner_create_email_template')
        if template:
            mails_sent = True
            for user in \
                self.env.ref('base.user_root').users:
                self._send_mail_mail(user, template.id, ctx)
                self._send_mail_notification(\
                    user, link_url, new_partner_is_supplier)
        return result

    # -------------------------- AUXILIARY METHODS ----------------------------

    def _send_mail_mail(self, user, template_id, ctx):
        if user.partner_id and user.partner_id.email:
            mails_sent = self.pool.get('email.template').send_mail(
                self.env.cr,
                self.env.ref('base.user_root').id,
                template_id,
                user.id,
                force_send=True,
                context=ctx)
        self._logger.info("Mails sent: %s" % mails_sent)

    def _send_mail_notification(self, user, link_url, is_supplier):
        if is_supplier is True:
            mail_message = self.env['mail.message'].create({
                'subject': 'New supplier',
                'body': '''
                <div>
                    A <a href="''' + link_url +
                    '''" target="_self">new supplier</a>''' +
                    ''' has been created.
                </div>
                <div>
                    To modify the new supplier click in this ''' +
                    '''<a href="''' + link_url  +
                    '''" target="_self">link</a>.
                </div>
                '''
            })
        else:
            mail_message = self.env['mail.message'].create({
                'subject': 'New customer',
                'body': '''
                <div>
                    A <a href="''' + link_url +
                    '''" target="_self">new customer</a>''' +
                    ''' has been created.
                </div>
                <div>
                    To modify the new customer click in this ''' +
                    '''<a href="''' + link_url  +
                    '''" target="_self">link</a>.
                </div>
                '''
            })
        self.env['mail.notification'].create({
            'message_id': mail_message.id,
            'partner_id': user.partner_id.id
        })

    def _get_base_url(self):
        """ Gets the URL in which Odoo is served; this can be set as an
            ir.config_parameter named `target.url` or live default which
            will be retrieved from openerp.http.request.httprequest.base_url
            :return (basestring): base URL
        """
        default = unicode(request.httprequest.host_url)
        param_domain = [('key', '=', 'target.url')]
        param_obj = self.env['ir.config_parameter']
        param_set = param_obj.search(param_domain)
        return u'{}/'.format(param_set.value) if param_set else default
